
### Summary

(Summarize the bug encountered concisely)

### Steps to reproduce

(How one can reproduce the issue - this is very important)

### What is the current *bug* behavior?

(What actually happens)

### What is the expected *correct* behavior?

(What you should see instead)

### Relevant logs and/or screenshots

(Paste the Xposed Installer error.log - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

### Vehicle Integration

(If you're using an integrated AA headunit in your vehicle, list the vehicle year/make/model)

### Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug
